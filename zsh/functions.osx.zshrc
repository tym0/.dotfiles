# Find everything
function finda () {
  find . -iname *$1*
}

# Convert Markdown to Word
function md2word () {
  pandoc -o $2 -f markdown -t docx $1
}

# Create basic web template
function webtemplate () {
  mkdir js && touch js/index.js
  mkdir css && touch css/index.css
  cd ./js
  wget https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
  cd ..
  touch index.html && echo '<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="css/index.css">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/index.js"></script>
</head>
<body>
  
</body>
</html>' >> index.html
}
