# Quick Look
alias qlf='qlmanage -p "$@" gt; /dev/null'

# Script to make Mac Update easy
alias masui="bash /users/Tim/Create/Projects/Prog/masui/masui"

# Airport utility
alias airport="/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport"

# VLC
alias vlc='/Applications/VLC.app/Contents/MacOS/VLC'

# Midnight Commander exit in folder
alias mc=". /usr/local/Cellar/midnight-commander/4.8.14/libexec/mc/mc-wrapper.sh"

# Lightning File Browser
alias li='python3 ~/Create/OtherGit/lightning-cd/lightning-cd.py ~/.lightningpath && cd "`cat ~/.lightningpath`"'

# Trash
alias th="trash"

# cd to WDI folders
alias cdhw="cd /Users/Tim/dev/WDI_LDN_17_HOMEWORK/timDeve"
alias cdcw="cd  /Users/Tim/dev/WDI_LDN_17_CLASSWORK"
alias cdln="cd /Users/Tim/dev/WDI_LDN_17_LESSON_NOTES"

# Copy Current Path
alias copp="pwd | pbcopy"

# Copy pullrequest format
alias prequest="cat ~/dev/pullRequestFormat.md | pbcopy"

# Shorter subl
alias s="subl"
